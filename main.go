package main

import (
	pb "gitlab.com/oleg.kuleba/goClient/protobuf"
	"gitlab.com/oleg.kuleba/goServer/dao"
	"log"
	"net"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"time"
	"fmt"
)

type server struct{}

const Port string = ":8282"
const Digit int = 250000

func main() {
	runServer()

	//dao.FindAll(Digit) // Метод для просмотра записей из boltDB
}

func runServer() {
	lis, err := net.Listen("tcp", Port)
	if err != nil {
		log.Fatalf("net.Listen() Error: %v", err)
	}

	serv := grpc.NewServer()
	pb.RegisterAddressBookServer(serv, &server{})
	reflection.Register(serv)
	if err := serv.Serve(lis); err != nil {
		log.Fatalf("serv.Serve() Error: %v", err)
	}
}

func (s *server) SendBook(stream pb.AddressBook_SendBookServer) error {
	startTime := time.Now()
	fmt.Println(startTime)

	//err := dao.UploadToDB(stream)
	err := dao.NewUploadToDB(stream)

	endTime := time.Now()
	fmt.Println(endTime)
	fmt.Println("Duration =", endTime.Sub(startTime))

	return err
}
