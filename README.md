# goServer

Cерверное GO-приложение. Принимает данные с клиента через stream и загружает их в БД boltDB

### Технологии: 
Go, Protobuf, gRPC, BoltDB.

### Загрузка проекта
Использовать любую из команд:<br>
1 - **go get gitlab.com/oleg.kuleba/goServer** с любой директории<br>
( если не сработает, то использовать комманду go get gitlab.com/oleg.kuleba/goServer.git и после нее изменить
 название папки проекта с goServer.git на goServer )<br>
или<br>
2 - **git clone https://gitlab.com/oleg.kuleba/goServer.git** в директории %GOPATH%\src\gitlab.com\oleg.kuleba

### Использование
- start<br>
В директории %GOPATH%\src\gitlab.com\oleg.kuleba\goServer выполнить команду go run main.go<br>
- stop<br>
Для остановки использовать сочетание клавиш Ctrl+C<br>
