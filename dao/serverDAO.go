package dao

import (
	"path/filepath"
	"log"
	"github.com/boltdb/bolt"
	"time"
	pb "gitlab.com/oleg.kuleba/goClient/protobuf"
	"io"
	"fmt"
	"github.com/golang/protobuf/proto"
)

const DBName string = "addressBook.db"
const ContactsBucketName string = "contacts"

type dbWorker func(tx *bolt.Tx) error

func dbPrepare(fn dbWorker, isWritableAction bool) error {
	// Получаем путь к БД
	path, err := filepath.Abs(DBName)
	if err != nil {
		log.Fatalf("did not find the path %v. Error: %v", path, err)
	}
	// Открываем БД
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 3 * time.Second})
	if err != nil {
		log.Fatalf("did not open DB %v. Error: %v", DBName, err)
	}
	defer db.Close()

	if isWritableAction {
		if err := db.Update(fn); err != nil { // Открываем Writable транзакцию
			return err
		}
	} else {
		if err := db.View(fn); err != nil { // Или Read-only транзакцию
			return err
		}
	}
	return nil
}

func UploadToDB(stream pb.AddressBook_SendBookServer) error {
	err := dbPrepare(func(tx *bolt.Tx) error {

		bucket, err := tx.CreateBucketIfNotExists([]byte(ContactsBucketName))
		if err != nil {
			log.Fatalf("tx.CreateBucketIfNotExists() Error: %v", err)
		}

		counter := 0
		for {
			request, err := stream.Recv()

			if err == io.EOF {
				return stream.SendAndClose(&pb.Response{
					Message: fmt.Sprintf("%s %d", "Read records:", counter),
				})
			}
			//fmt.Println(request.Phone)
			//fmt.Println(request.Contact)

			if err != nil {
				log.Fatalf("stream.Recv() Error: %v", err)
			}
			if contact, err := proto.Marshal(request); err == nil {
				if err := bucket.Put([]byte(request.Phone.Number), contact); err != nil {
					log.Fatalf("bucket.Put() Error: %v", err)
				}
			} else {
				log.Fatalf("proto.Marshal() Error: %v", err)
			}
			counter++
			if counter%1000000 == 0 {
				fmt.Println("written records:", counter)
				if err := tx.Commit(); err != nil {
					log.Fatalf("tx.Commit() Error: %v", err)
				}
				//tx = &bolt.Tx{}
			}
		}
	}, true) // Открываем Writable транзакцию
	return err
}

func NewUploadToDB(stream pb.AddressBook_SendBookServer) error {
	path, err := filepath.Abs(DBName)
	if err != nil {
		log.Fatalf("did not find the path %v. Error: %v", path, err)
	}
	// Открываем БД
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 3 * time.Second})
	if err != nil {
		log.Fatalf("did not open DB %v. Error: %v", DBName, err)
	}
	defer db.Close()

	tx, err := db.Begin(true)
	if err != nil {
		log.Fatalf("db.Begin(true) Error: %v", err)
	}


	//=====================================================================
	counter := 0
	for {
		bucket, err := tx.CreateBucketIfNotExists([]byte(ContactsBucketName))
		if err != nil {
			log.Fatalf("tx.CreateBucketIfNotExists() Error: %v", err)
		}

		request, err := stream.Recv()

		if err == io.EOF {
			if err := tx.Commit(); err != nil {
				log.Fatalf("tx.Commit() Error: %v", err)
			}
			return stream.SendAndClose(&pb.Response{
				Message: fmt.Sprintf("%s %d", "Read records:", counter),
			})
		}


		if err != nil {
			log.Fatalf("stream.Recv() Error: %v", err)
		}
		if contact, err := proto.Marshal(request.Contact); err == nil {
			if err := bucket.Put([]byte(request.Phone.Number), contact); err != nil {
				log.Fatalf("bucket.Put() Error: %v", err)
			}
		} else {
			log.Fatalf("proto.Marshal() Error: %v", err)
		}
		counter++
		if counter%5000000 == 0 {
			fmt.Println(request.Phone)
			fmt.Println(request.Contact)

			fmt.Println("written records:", counter)
			if err := tx.Commit(); err != nil {
				log.Fatalf("tx.Commit() Error: %v", err)
			}
			tx, err = db.Begin(true)
			if err != nil {
				log.Fatalf("db.Begin(true) Error: %v", err)
			}
		}
	}
	//=====================================================================
	return err
}

func FindAll(digit int) error {
	err := dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			cursor := bucket.Cursor()
			if k, _ := cursor.First(); k == nil { // Если cursor баккета пустой (ключей/записей нет) - сообщаем об этом и выходим
				fmt.Println("DB is empty")
				return nil
			}
			counter := 0
			for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
				phone := &pb.Phone{
					Number: string(k),
				}
				contact := &pb.Contact{}
				if err := proto.Unmarshal(v, contact); err != nil {
					return err
				}
				if counter % digit == 0 {
					fmt.Println(phone)
					fmt.Println(contact)
				}
				counter++
			}
		}
		return nil
	}, false) // Открываем Read-only транзакцию
	return err
}
